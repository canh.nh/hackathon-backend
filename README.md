## Usage

To use first ensure that you have
[Docker compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04) 
available.

```bash
docker-compose up --build
docker-compose run app python manage.py migrate
docker-compose logs -f web
```

## Terminal in docker
```bash
docker-compose run app
```

## Run test

```bash
docker-compose run app python manage.py test
```


## Install new package

```bash
docker-compose run app poetry add {package}
```

## Install new package in pyproject.toml from other branches

```bash
docker-compose run app poetry install
```

## Update all packages

```bash
docker-compose run app poetry update
```

## Run container command

```bash
docker-compose run app python manage.py {command}
```
