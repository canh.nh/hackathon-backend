from django.contrib.auth import authenticate
from django.utils.translation import gettext
from rest_framework import serializers
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from accounts.serializers.user import UserSerializer


class TokenLoginSerializer(serializers.Serializer):
    password = serializers.CharField()
    username = serializers.CharField()


class LoginResponseSerializer(serializers.Serializer):
    token = serializers.CharField(source='key')
    user = UserSerializer()


class LoginApi(GenericAPIView):
    serializer_class = TokenLoginSerializer
    response_serializer_class = LoginResponseSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = authenticate(request, **serializer.validated_data)

        if not user:
            return Response(
                {
                    'message': gettext('Incorrect username or password'),
                }, status=status.HTTP_400_BAD_REQUEST,
            )

        token, _ = Token.objects.get_or_create(user=user)
        response_serializer = self.response_serializer_class(instance=token)
        return Response(response_serializer.data)

