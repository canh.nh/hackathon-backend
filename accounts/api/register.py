from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from accounts.serializers.register import RegisterSerializer
from accounts.serializers.user import UserSerializer


class RegisterApi(GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.process_register_member()
        return Response(UserSerializer(user).data)
