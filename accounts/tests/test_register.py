from django.test import TestCase
from django.test import Client
from rest_framework import status

from accounts.models import User


class RegisterPhoneTests(TestCase):
    def setUp(self):
        super().setUp()
        self.client = Client()

    def send_register(self, data, **extra):
        response = self.client.post(
            '/api/accounts/register/', data,
            format='json',
            **extra,
        )
        return response

    def test_register_account(self):
        data = {
            'username': '0901234569',
            'name': 'test_name',
            'email': 'email_test@gmail.com',
            'phone_number': '0901234569',
            'password1': 'jkasdfbj',
            'password2': 'jkasdfbj',
        }
        response = self.send_register(data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        user = User.objects.get(username='0901234569')
        self.assertEqual(user.name, 'test_name')
