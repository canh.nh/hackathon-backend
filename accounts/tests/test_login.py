from django.test import TestCase
from django.test import Client
from rest_framework import status
from rest_framework.authtoken.models import Token
from accounts.factories.user import UserFactory
from rest_framework.test import APITestCase

class UserAuthTests(APITestCase):
    def setUp(self):
        super().setUp()


    def test_user_login_success(self):
        self.user = UserFactory()
        response = self.client.post(
            '/api/accounts/login/', {
                'username': self.user.username,
                'password': self.user.raw_password,
            },
            format='json',
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        token = Token.objects.get(user=self.user)
        self.assertEqual(response.data['token'], token.key)
        self.assertEqual(response.data['user']['name'], self.user.name)
