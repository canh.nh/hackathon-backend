from django.contrib.auth.admin import UserAdmin as DjUserAdmin
from django.utils.translation import gettext_lazy as _


class UserAdmin(DjUserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (
            _('Personal info'), {
                'fields': (
                    'email',
                    'phone_number',
                ),
            },
        ),
        (
            _('Permissions'), {
                'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
            },
        ),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    ordering = ('email',)
    search_fields = ('username', 'email')
    add_fieldsets = (
        (
            None, {
                'classes': ('wide',),
                'fields': ('username', 'password1', 'password2', 'is_manager'),
            },
        ),
    )
