import factory

from accounts.models import User


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('user_name')
    email = factory.Sequence(lambda n: f'email{n}@eoh.io')
    phone_number = factory.Faker('phone_number')
    name = factory.Faker('name')
    password = factory.Faker('password')

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        user = super(UserFactory, cls)._create(model_class, *args, **kwargs)
        password = kwargs['password']
        user.set_password(password)
        user.raw_password = password
        user.save()
        return user
