from django.urls import path

from accounts.api.facebook_login import FacebookLoginView
from accounts.api.google_login import GoogleLoginView
from accounts.api.login_api import LoginApi
from accounts.api.register import RegisterApi

urlpatterns = [
    path('login/', LoginApi.as_view()),
    path('register/', RegisterApi.as_view()),
    path('login/facebook/', FacebookLoginView.as_view()),
    path('login/google/', GoogleLoginView.as_view()),
]
