from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager
from django.db import models
from django.db.models.manager import BaseManager


class UserQuerySet(models.QuerySet):
    pass


class CustomUserManager(BaseManager.from_queryset(UserQuerySet), UserManager):
    pass


class User(AbstractUser):
    name = models.CharField(max_length=80, blank=True, null=True)
    phone_number = models.CharField(max_length=50, unique=True, null=True)

    objects = CustomUserManager()

    def set_password(self, raw_password):
        super(User, self).set_password(raw_password)
