from django.conf import settings
from django.utils.translation import gettext
from rest_framework import serializers

from accounts.models import User


class RegisterSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField()
    password2 = serializers.CharField()
    phone_number = serializers.CharField()

    class Meta:
        model = User
        fields = (
            'username',
            'name',
            'email',
            'password1',
            'password2',
            'phone_number'
        )

    def validate_phone_number(self, value):
        if User.objects.filter(phone_number=value).exists():
            raise serializers.ValidationError('Phone number already in use')
        return value

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError('Email address already in use')
        return value

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs['password1'] != attrs['password2']:
            raise serializers.ValidationError(gettext("The two password fields didn't match."))
        attrs['password'] = attrs.pop('password1')
        attrs.pop('password2')
        return attrs

    def process_register_member(self):
        user = User(
            is_active=True,
            **self.validated_data,
        )
        user.set_password(self.validated_data['password'])
        user.save()

        return user
